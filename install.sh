#!/bin/bash

BASEDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

mkdir -p ~/.vim/plugged
mkdir -p ~/.config/nvim
mkdir -p ~/.config/nvim/lua

# nvim
#sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \'
#ln -s ${BASEDIR}/.config/nvim/coc-settings.json ~/.config/nvim/coc-settings.json
ln -s ${BASEDIR}/.config/nvim/init.vim ~/.config/nvim/init.vim
ln -s ${BASEDIR}/.config/nvim/lua/init.lua ~/.config/nvim/lua/init.lua

# vim (We keep the same comfig file as neovim)
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
ln -s ${BASEDIR}/.config/nvim/init.vim ~/.vimrc

# zsh
#ln -s ${BASEDIR}/.zshrc ~/.zshrc

# ssh
ln -s ${BASEDIR}/.ssh/config ~/.ssh/config


