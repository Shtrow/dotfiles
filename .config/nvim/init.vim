" Specify a directory for plugins
"~/.config/nvim/lua/init.lua
"C:\Users\Benjamin\AppData\Local\nvim\lua\init.lua"
if has('nvim')
    lua require('init')
else
call plug#begin('~/.vim/plugged')
" ** C:\Users\Benjamin\dotfiles\.config\nvim\lua\lsp_config.lua
" Plug 'ervandew/supertab'        " Use Tab
Plug 'Townk/vim-autoclose'      " Autoclose pairs
Plug 'Procrat/oz.vim'
"Plug 'jiangmiao/auto-pairs'
Plug 'airblade/vim-gitgutter'   " Git changes
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
"Plug 'sheerun/vim-polyglot'     " Language support
Plug 'godlygeek/tabular'        " Align
Plug 'scrooloose/nerdcommenter' " Comment code
Plug 'ctrlpvim/ctrlp.vim'       " Fuzzy finder
"Plug 'neoclide/coc.nvim',{'branch': 'release'} 	" Snippet and additional text editing 
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'	" Git icones in nerdtree

Plug 'morhetz/gruvbox'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'OmniSharp/omnisharp-vim' " OmniSharp
Plug 'jpalardy/vim-slime' " Slime -> Run code
Plug 'habamax/vim-godot' " Godot 
Plug 'tikhomirov/vim-glsl' " GLSL highlight and syntax
" Initialize plugin system
call plug#end()

" Shortcuts
tnoremap <Esc> <C-\><C-n>
inoremap jk <Esc>
inoremap kj <Esc>
nnoremap <C-j> :cnext<CR>
nnoremap <C-k> :cprevious<CR>

inoremap <silent><expr> <TAB> pumvisible() ? "\<C-n>" : "\<TAB>"
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

"function! CleverTab()
   "if strpart( getline('.'), 0, col('.')-1 ) =~ '^\s*$'
	  "return "\<Tab>"
   "else
	  "return "\<C-N>"
   "endif
"endfunction
"inoremap <Tab> <C-R>=CleverTab()<CR>
" ** Setup indentation **
set smartindent                                  " Smart indentation
set autoindent                                   " Keep indentation with new lines
set wrap                                         " Return at the end of a line
set tabstop=2                                    " Size of a tab when opening file
set shiftwidth=2                                 " Shift with 2 spaces
set expandtab                                    " Change tabulations in spaces
"set colorcolumn=80                               " Set guidline for 80 columns

" ** System clipboard
set clipboard=unnamed                             " Copy into system clipboard

" ** Buffers setup **
set autowrite
" Triger `autoread` when files changes on disk
" https://unix.stackexchange.com/questions/149209/refresh-changed-content-of-file-opened-in-vim/383044#383044
" https://vi.stackexchange.com/questions/13692/prevent-focusgained-autocmd-running-in-command-line-editing-mode
    autocmd FocusGained,BufEnter,CursorHold,CursorHoldI *
            \ if mode() !~ '\v(c|r.?|!|t)' && getcmdwintype() == '' | checktime | endif

" Notification after file change
" https://vi.stackexchange.com/questions/13091/autocmd-event-for-autoread
autocmd FileChangedShellPost *
  \ echohl WarningMsg | echo "File changed on disk. Buffer reloaded." | echohl None

" Settings
set autoindent                                   " Keep indentation with new lines
set mouse=a                                      " Mouse support
set nohlsearch
set ignorecase
set smartcase
set incsearch
set scrolloff=4
augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber number
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

" REMOTE
"call serverstart('\\.\pipe\nvim-1234')

"CTRLP
let g:ctrlp_cmd = 'CtrlPLastMode'

" ** OCaml settings **
set sb
map <F6> :vsplit /tmp/ocaml \| %d \|setlocal ft=omlet \| setlocal autowrite \| r!ocaml < # <CR>
map <F7> :dr /tmp/ocaml \| %d \|setlocal ft=omlet \|setlocal autowrite \| r!ocaml < # <CR>
"
"
" Customization
let g:gruvbox_italic=1
let g:gruvbox_contrast_light='soft'
let g:gruvbox_contrast_dark='hard'
set termguicolors
let g:airline_theme='gruvbox'
colorscheme gruvbox


" Heptagon Comment
let g:NERDCustomDelimiters = { 'ept': { 'left': '(*','right': '*)' } }
"nnoremap <C-/> I(*<esc>A*)<esc>
"autocmd FileType ept nnoremap <leader>cc I(*<esc>A*)<esc>
map <silent> <C-/> :call nerdcommenter#Comment(0,'toggle')<CR>


" See diff between buffer and file
function! s:DiffWithSaved()
  let filetype=&ft
  diffthis
  vnew | r # | normal! 1Gdd
  diffthis
  exe "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
endfunction
com! DiffSaved call s:DiffWithSaved()

"Godot config
set noet ci pi sts=0 sw=4 ts=4

"nnoremap <F5> :w|!godot<CR>

nmap <F5> :!godot <enter>
if (&ft == 'gd')
	nmap <F5> :!godot <enter>
endif
endif

autocmd QuickFixCmdPost [^l]* nested cwindow
autocmd QuickFixCmdPost    l* nested lwindow
